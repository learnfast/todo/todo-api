package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/gorilla/mux"
	// "fmt"
	// "net/http"
	// "net/http/httptest"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "time"
	// "context"
	// "io/ioutil"
	// "math/rand"
	// "encoding/json"
	// "bytes"
	// "strconv"
	// "os"
)


func TestExample(t *testing.T){
	assert.Equal(t,1,1,"OK")
}

func Router() *mux.Router {
    router := mux.NewRouter()
	router.HandleFunc("/list", GetListEndpoint).Methods("GET")
	router.HandleFunc("/task", CreateTaskEndpoint).Methods("POST")
    return router
}


// func TestGetListEndpoint(t *testing.T) {
// 	host := os.Getenv("MONGODB_SERVICE_SERVICE_HOST")
// 	port := os.Getenv("MONGODB_SERVICE_SERVICE_PORT")
// 	username := os.Getenv("MONGODB_USERNAME")
// 	password := os.Getenv("MONGODB_PASSWORD")
// 		clientOptions := options.Client().ApplyURI("mongodb://" + username + ":" + password + "@" + host + ":" + port)
// 	// clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
// 	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
// 	client, _ = mongo.Connect(ctx, clientOptions)
//   request, _ := http.NewRequest("GET", "/list", nil)
//   response := httptest.NewRecorder()
//   Router().ServeHTTP(response, request)

// 	bodyBytes, err := ioutil.ReadAll(response.Body)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	bodyString := string(bodyBytes)
// 	fmt.Println(bodyString)


// 	assert.Equal(t, 200, response.Code, "OK response is expected")
// }


// func TestCreateTask(t *testing.T) {
// 	s1 := rand.NewSource(time.Now().UnixNano())
// 	r1 := rand.New(s1)
// 	randInt := r1.Intn(1000000)
// 	randStr := strconv.Itoa(randInt)
// 	postBody, _ := json.Marshal(map[string]string{
// 		"task": randStr ,
// 	})
// 	RandGeneratedTask := bytes.NewBuffer(postBody)
// 	host := os.Getenv("MONGODB_SERVICE_SERVICE_HOST")
// 	port := os.Getenv("MONGODB_SERVICE_SERVICE_PORT")
// 	username := os.Getenv("MONGODB_USERNAME")
// 	password := os.Getenv("MONGODB_PASSWORD")
// 	clientOptions := options.Client().ApplyURI("mongodb://" + username + ":" + password + "@" + host + ":" + port)
// 	// clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
// 	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
// 	client, _ = mongo.Connect(ctx, clientOptions)
//     request, _ := http.NewRequest("POST", "/task", RandGeneratedTask)
//     response := httptest.NewRecorder()
//     Router().ServeHTTP(response, request)

// 	request, _ = http.NewRequest("GET", "/list", nil)
//     response = httptest.NewRecorder()
//     Router().ServeHTTP(response, request)

// 	bodyBytes, err := ioutil.ReadAll(response.Body)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	bodyString := string(bodyBytes)
// 	fmt.Println(bodyString)

// 	assert.Containsf(t, bodyString, randStr, "OK response is expected")
// }
