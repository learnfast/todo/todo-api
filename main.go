package main

import (
	"context"
	"fmt"
	"net/http"
	"time"
	"log"
	"os"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client

type ToDos struct {
	Task string `json:"task,omitempty" bson:"task,omitempty"`
}


func RootEndpoint(response http.ResponseWriter, request *http.Request){
	response.Write([]byte("TODO!!!"))
}

func CreateTaskEndpoint(response http.ResponseWriter, request *http.Request){
	response.Header().Add("content-type","application/json")
	var task ToDos
	json.NewDecoder(request.Body).Decode(&task)
	collection := client.Database("todolist").Collection("list")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	result, _ := collection.InsertOne(ctx,task)
	json.NewEncoder(response).Encode(result)
}

func GetListEndpoint(response http.ResponseWriter, request *http.Request){
	response.Header().Add("content-type", "application/json")
	var list []ToDos
	collection := client.Database("todolist").Collection("list")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Header().Set("Access-Control-Allow-Origin", "*")
		response.Write([]byte(`{ "message": "`+ err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var task ToDos
		cursor.Decode(&task)
		list = append(list, task)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "`+ err.Error() + `"}`))
		return
	}
	json.NewEncoder(response).Encode(list)
}


func main(){
	fmt.Println("Starting the application...")
	host := os.Getenv("MONGODB_SERVICE_SERVICE_HOST")
	port := os.Getenv("MONGODB_SERVICE_SERVICE_PORT")
	username := os.Getenv("MONGODB_USERNAME")
	password := os.Getenv("MONGODB_PASSWORD")
	clientOptions := options.Client().ApplyURI("mongodb://" + username + ":" + password + "@" + host + ":" + port)
	// clientOptions := options.Client().ApplyURI(URI())
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ = mongo.Connect(ctx, clientOptions)
	router := mux.NewRouter()
	router.HandleFunc("/task", CreateTaskEndpoint).Methods("POST")
	router.HandleFunc("/list", GetListEndpoint).Methods("GET")
	router.HandleFunc("/", RootEndpoint)
	log.Fatal(http.ListenAndServe(":12345", handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(router)))
}
