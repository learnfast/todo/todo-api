FROM golang
WORKDIR /go/src/app
COPY . .
RUN go mod download
RUN go build -o bin/main main.go
EXPOSE 12345
CMD ["bin/main"]
