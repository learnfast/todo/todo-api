# ToDo Project / REST API

## Project Overview

---

This Project is using for list the tasks from the database and create tasks and write into the database for ToDo List.

## Usage

---

- `[IP Address]:[Port]/list` will list the all tasks with GET request
- `[IP Address]:[Port]/taks` will add new task to the database with POST Request.

### cURL Example

```sh
curl -X GET  -I http://apitodo.servisimyolda.net/list
```

###

```sh
curl -X POST -H "Content-Type: application/json" -d  '{"task": "something"}'  http://apitodo.servisimyolda.net/task
```
